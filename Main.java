// Un fichier java commence toujours par le package dans lequel se situe le fichier source.
// ATTENTION si vous mettez ce package il faut que votre fichier soit dans le dossier src/main/java/fr/imie avec le nom Main.java
package fr.imie;

// Ensuite viens les imports des classes nécessaires à la compilation du code. 
// Pour trouver les packages des classes, il faut utiliser la javadoc ou l'autocomplétion de son IDE.
// Exemple : import java.util.ArrayList;

// Enfin on finit par la déclaration de la classe
public class Main {

    // Le point d'entrée de mon application
    public static void main(String[] args) {
        // Pour éviter d'être dans un contexte static on instantie un objet et on appelle sa méthode run
        new Main().run();
    }

    public void run() {
        int annee = 2016;

        if ((annee%4 == 0 && annee%100 != 0) || annee%400 == 0){
            System.out.println("Année bissextile !");
        }
        else {
            System.out.println("Année non bissextile !");
        }
    }
}